import React from 'react';

// Create a Context Object
// >> it is a data type of an object tha we will use to store information that can be shared to otehr components within the app.
const UserContext = React.createContext();

// this part will be the provider that will give the data to consume by other components and supply throughtout the app.
// This will be the one to import in the app.js that's why it is wrap in {} in app.js
export const UserProvider = UserContext.Provider;

// Then UserContext will be the one to be imported throughout the other components adn to use it import too the {useContext}
export default UserContext;