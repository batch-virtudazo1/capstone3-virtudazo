import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
export default function Logout() {
	 const {unsetUser, setUser} = useContext(UserContext);
	// clear out the inputs
	// localStorage.clear();
	unsetUser();
	// placing the value of setUser using useEffect
	useEffect(() => {
		setUser({id: null})
	})
	return (
			<Navigate to = "/login"/>
		)

}