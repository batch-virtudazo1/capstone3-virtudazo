import {useContext, useEffect, useState} from 'react';
import {Link, Navigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
export default function Login() {
	
	// Allows us to consume UserContext obj and it's properties (you can choose what properties to use like user or setUser, or unsetUser).
	const {user, setUser} = useContext(UserContext);
	console.log(user);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function loginUser(e) {
		e.preventDefault();

		fetch('https://secret-meadow-31644.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to booking App 196!"
				})
			} else {
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your credentials"
				})
			}
		})
		
		setEmail('');
		setPassword('');
		
	}

	const retrieveUserDetails = (token) => {
		fetch('https://secret-meadow-31644.herokuapp.com/users/getUserDetails', {
			// here, we did not put method since the default method is the same as the route which is GET.
			headers: {
				Authorization: `Bearer ${token}`
			}

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	};

	useEffect(() => {

		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [email, password]);

	return (

		(user.id !== null) ?
		<Navigate to = "/products/"/>
		:

		<>
			<h1>Login:</h1>
			<Form onSubmit = {e => loginUser(e)}>
				<Form.Group controlId = "userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control 
						type = "email"
						placeholder = "Enter your email"
						required
						value = {email}
						onChange = {e => setEmail(e.target.value)}
					/>
				</Form.Group>

				<Form.Group controlId = "password">
					<Form.Label>Password</Form.Label>
					<Form.Control 
						type = "password"
						placeholder = "Enter your password"
						required
						value = {password}
						onChange = {e => setPassword(e.target.value)}
					/>
				</Form.Group>
				<p>Not yet registered?<Link to = "/register">Register here</Link></p>
				{ isActive ?
					<Button className = "mt-3 mb-5" variant = "success" type = "submit" id ="submitBtn">
						Login
					</Button>
					:
					<Button className = "mt-3 mb-5" variant = "danger" type = "submit" id ="submitBtn" disabled>
						Login
					</Button>
				}
			</Form>
		</>
	)
}


