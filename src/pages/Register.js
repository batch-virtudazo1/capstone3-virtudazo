import {useContext, useEffect, useState} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
export default function Register() {
	
	const {user} = useContext(UserContext);
	const history = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);


	function registerUser(e) {
		e.preventDefault();

		fetch('https://secret-meadow-31644.herokuapp.com/users/checkEmailExists',
		{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Duplicate email found",
					icon: "info",
					text: "The email that you are trying to register is already exist."
				})
			} else {
				fetch('https://secret-meadow-31644.herokuapp.com/users',{
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						email: email,
						password: password,
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data){
						Swal.fire({
							title: 'Registration Successful!',
							icon: 'success',
							text: 'Thank you for registering'
						});
						history("/login");
					} else {
						Swal.fire({
							title: 'Registration failed',
							icon: 'error',
							text: 'Something went wrong, try again'
						});
					}
				})

			}
		})

		setEmail('');
		setPassword('');
		setFirstName('');
		setLastName('');
		setMobileNo('');


		}
	useEffect(() => {

		if(firstName !== '' && lastName !== '' && mobileNo !== '' && mobileNo.length === 11 && email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [firstName, lastName, mobileNo, email, password]);

	return (
		(user.id !== null) ?
			<Navigate to = "/products/"/>
		:

		<>
			<h1>Register Here:</h1>
			<Form onSubmit = {e => registerUser(e)}>
				
				<Form.Group controlId = "firstName">
					<Form.Label>First Name:</Form.Label>
					<Form.Control 
						type = "text"
						placeholder = "Enter your first name"
						required
						value = {firstName}
						onChange = {e => setFirstName(e.target.value)}
					/>
				</Form.Group>

				<Form.Group controlId = "lastName">
					<Form.Label>Last Name:</Form.Label>
					<Form.Control 
						type = "text"
						placeholder = "Enter your last name"
						required
						value = {lastName}
						onChange = {e => setLastName(e.target.value)}
					/>
				</Form.Group>

				<Form.Group controlId = "mobile">
					<Form.Label>Mobile Number:</Form.Label>
					<Form.Control 
						type = "text"
						placeholder = "Enter your 11-digit mobile number"
						required
						value = {mobileNo}
						onChange = {e => setMobileNo(e.target.value)}
					/>
				</Form.Group>

				<Form.Group controlId = "userEmail">
					<Form.Label>Email Address:</Form.Label>
					<Form.Control 
						type = "email"
						placeholder = "Enter your email"
						required
						value = {email}
						onChange = {e => setEmail(e.target.value)}
					/>
					<Form.Text className = "text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>

				<Form.Group controlId = "password">
					<Form.Label>Password:</Form.Label>
					<Form.Control 
						type = "password"
						placeholder = "Enter your password"
						required
						value = {password}
						onChange = {e => setPassword(e.target.value)}
					/>
				</Form.Group>

				{ isActive ?
					<Button className = "mt-3 mb-5" variant = "success" type = "submit" id ="submitBtn">
						Register
					</Button>
					:
					<Button className = "mt-3 mb-5" variant = "danger" type = "submit" id ="submitBtn" disabled>
						Register
					</Button>
				}
			</Form>
			</>
		)
}