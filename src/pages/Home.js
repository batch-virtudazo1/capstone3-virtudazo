import {Image} from 'react-bootstrap';
import Banner from '../components/Banner';
import WallImage from '../images/wall.jpg';
import Highlights from '../components/Highlights';


export default function Home() {
	return (
			<>
				<Banner/>
				<Highlights/>
			</>
		)
}