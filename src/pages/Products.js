import {useContext, useEffect, useState} from 'react';
import {Navigate} from 'react-router-dom';
import AdminProductsView from '../components/AdminProductsView';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products() {
	
	const {user, setUser} = useContext(UserContext);
	console.log(user);
	
	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch("https://secret-meadow-31644.herokuapp.com/products/getAllActiveProducts")
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product =>{
				return(
					
					<ProductCard key={product._id} productProp = {product}/>
					
					)
			}));

		})
	}, [])

	return (
			(user.isAdmin === false || user.id === null)?
				<>
				<h1>Available Items</h1>
				{products}
				</>
			:
			
			<AdminProductsView/>
	)
}