import {useContext} from 'react';
import {Navigate} from 'react-router-dom';
import AdminProfile from '../components/AdminProfile';
import UserProfile from '../components/UserProfile';
import UserContext from '../UserContext';


export default function Profile(){


	const {user, setUser} = useContext(UserContext);
	console.log(user);
	
	return(
		(user.id !== null)?
		(user.isAdmin === true )?
		<AdminProfile/>
		:
		<UserProfile/>
		:
		<Navigate to = "/register/"/>
	)
}