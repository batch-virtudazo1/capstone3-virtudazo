import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Error() {

	
	return (
		<>
		<h1>404 Not Found</h1>
		<p>The page you are looking for is not found.</p>
		<Button variant = 'primary' as = {Link} to = '/'>Back to Home</Button>
		</>
		)
}