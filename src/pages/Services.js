import {useContext} from 'react';
import {Navigate} from 'react-router-dom';
import {Form, Button, FloatingLabel, Table} from 'react-bootstrap';
import UserContext from '../UserContext'


export default function Services(){

	const {user, setUser} = useContext(UserContext);
	console.log(user);
	
	return(
			(user.id !== null)?
			(user.isAdmin !== true)?
			<>
			<h1>Personalized Service:</h1>
			<Form>
				

				<Form.Label>What outfit:</Form.Label>
				<Form.Select aria-label="Default select example" className = "mb-3">
				      <option>Choose what outfit</option>
				      <option value="1">Tops</option>
				      <option value="2">Bottoms</option>
				      <option value="3">Head wear</option>
				      <option value="4">Bags</option>
				      <option value="5">Accessories</option>
				      <option value="6">Costume/</option>
				</Form.Select>

				<Form.Label>Type of service:</Form.Label>
				<Form.Select aria-label="Default select example" className = "mb-3">
				      <option>Chose what service:</option>
				      <option value="1">Repair</option>
				      <option value="2">Dress make</option>
				      <option value="3">Create costume</option>
				     
				</Form.Select>

				<Form.Group controlId = "password" className = "mb-3">
					<Form.Label>Your Contact number:</Form.Label>
					<Form.Control 
						type = "password"
						placeholder = "Enter your 11-digit number here"
						required					
					/>
				</Form.Group>
				
				<Form.Label>Service Specific Required Details:</Form.Label>
				<FloatingLabel controlId="floatingTextarea2" label = "Needed details are: Sizes, Features on the item, Materials. Other info will be ask after the dressmaker reached you out." className="text-muted"> 
			 	<Form.Control
			    as="textarea"
			    placeholder="Please input your request service here."
			    
			  	/>
				</FloatingLabel>
			</Form>
			</>
			:
			<>
			<h1 className = "text-center">Services</h1>
			<Table striped bordered hover className="m-auto">
			      <thead>
			        <tr>
			          <th>Type of Service</th>
			          <th>Customer's Name</th>
			          <th>Customer's Contact</th>
			          <th>Service Details</th>
			        </tr>
			      </thead>
			      <tbody>
			        <tr>
			          <td>Repair</td>
			          <td>Aries Hans</td>
			          <td>09090909090</td>
			          <td>"Repair my pants"</td>
			        </tr>
			      </tbody>
			</Table>
			</>
			:
			<Navigate to = "/register/"/>
		)
}