import {useEffect, useState} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Error from './pages/Error';
import Home from './pages/Home';
import Products from './pages/Products';
import Profile from './pages/Profile';
import Services from './pages/Services';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ProductView from './components/ProductView';
import Register from './pages/Register';
import './App.css';
import {UserProvider} from './UserContext';

function App() {

    // state hook for the user state that defined here for a global scope. Initialized as an object with properties from the local storage.
    // The user here is loggined
      const [user, setUser] = useState({
        id: null,
        isAdmin: null
      });

      const unsetUser = () => {
        localStorage.clear();
      };

      useEffect(() => {
        fetch('https://secret-meadow-31644.herokuapp.com/users/getUserDetails', {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
        .then(res => res.json())
        .then(data => {
          // captured the user details loggined. like in the login page
          console.log(data);
          if(typeof data._id !== "undefined"){
            setUser({
              id: data._id,
              isAdmin: data.isAdmin

            })
          } else {
            setUser({
              id: null,
              isAdmin:null
            })
          }

        })

      }, []);

 return (

    <UserProvider value = {{user, setUser, unsetUser}}>
          <Router>
            <AppNavBar/>
            <Container>
                <Routes>
                    <Route exact path = "/" element = {<Home/>}/>
                    <Route exact path = "/products" element = {<Products/>}/>
                    <Route exact path = "/productView/:productId" element = {<ProductView/>}/>
                    <Route exact path = "/services" element = {<Services/>}/>
                    <Route exact path = "/profile" element = {<Profile/>}/>
                    <Route exact path = "/login" element = {<Login/>}/>
                    <Route exact path = "/logout" element = {<Logout/>}/>
                    <Route exact path = "/register" element = {<Register/>}/>
                    <Route exact path = "*" element = {<Error/>}/>

                </Routes>
            </Container>
          </Router>
    </UserProvider>
   )
}

export default App;
