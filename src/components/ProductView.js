import {useContext, useEffect, useState} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {Link, useParams, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {

	const {user} = useContext(UserContext);

	// allows us to redirect a user to a different page after enrolling
	const history = useNavigate();

	const [name, setName] =useState("");
	const [description, setDescription] =useState("");
	const [price, setPrice] =useState(0);
	const [quantity, setQuantity] =useState(1);

	// allows us to use productId passed via the url
	const {productId} = useParams();
	useEffect(() =>{
		console.log(productId)
		fetch(`https://secret-meadow-31644.herokuapp.com/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	},[productId]);

	const buy = (productId) => {

		fetch('https://secret-meadow-31644.herokuapp.com/users/checkoutOrders', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				//productId: productId
				totalAmount: price,
				products: [
					 {
						productId: productId,
						quantity: quantity,
						productName: name
					}
				]
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			console.log(quantity);				
			if(data){
				Swal.fire({
					title: 'Successfully place the order',
					icon: 'success',
					text: 'Thank you for placing an order!'
				});
				history("/products");

			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				});
			}

		})
	}



	return(
			<Container className = "mt-5">
				<Row>
					<Col lg ={{span:6, offset:3}}>
						<Card>
							<Card.Body>
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>Php {price}</Card.Text>
								
								{ user.id !== null ?
									<Button variant = "primary" onClick = {() => buy(productId)}>Buy</Button>
									:
									<Link className = "btn btn-danger" to = "/login">Log in</Link>
								}
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		)
}