import {useState} from 'react';
import {Button,  Form, Modal, } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminProductsViewCard({productsProp}) {

	console.log(productsProp);
	const { name, description, price, isActive, _id} = productsProp;
	

	const [modalName, setModalName] = useState(name);
	const [modalDescription, setModalDescription] = useState(description);
	const [modalPrice, setModalPrice] = useState(price);

	const [itemName, setItemName] = useState(modalName);
	const [itemDescription, setItemDescription] = useState(modalDescription);
	const [itemPrice, setItemPrice] = useState(modalPrice);
	const [itemIsActive, setItemIsActive] = useState(isActive);
	const [deleteItem, setDeleteItem] = useState(false)



	
	// useState for modals
	const [showEdit, setShowEdit] = useState(false);
	const [showDelete, setShowDelete] = useState(false);


	// const for modals
	const handleCloseEdit = () => {
		setShowEdit(false);
		setModalName(itemName)
		setModalDescription(itemDescription)
		setModalPrice(itemPrice)
	}
	const handleShowEdit = () => setShowEdit(true);

	const handleCloseDelete = () => setShowDelete(false);
	const handleShowDelete = () => setShowDelete(true);


	console.log(name)
	console.log(description)
	console.log(price)

	// for no forcing the page to refresh when updating the status
	let status = 'Available'

	if(isActive){
		status = 'Available'
	} else {
		status = 'Not Available'
	}
	
	// useState for status check
	const [itemIsActiveWord, setItemIsActiveWord] = useState(status);



	function editProduct(_id){

		fetch(`https://secret-meadow-31644.herokuapp.com/products/updateProduct/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: modalName,
				description: modalDescription,
				price: modalPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			Swal.fire({
				title: 'Successful',
				icon: 'success',
				text: 'Product Update!'
			});
			setModalName(data.name)
			setModalDescription(data.description)
			setModalPrice(data.price)

			setItemName(modalName)
			setItemDescription(modalDescription)
			setItemPrice(modalPrice)
			
		})
		// setItemName(modalName)
		// setItemDescription(modalDescription)
		// setItemPrice(modalPrice)

	}

	function deleteProduct(_id){

		fetch(`https://secret-meadow-31644.herokuapp.com/products/deleteProduct/${_id}`, {
			method: 'DELETE',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			Swal.fire({
				title: 'Successful',
				icon: 'success',
				text: 'Item Deleted!'
			});
			setShowDelete(false)
			setDeleteItem(true)
		})
	}


	function activateStatus(_id){
		fetch(`https://secret-meadow-31644.herokuapp.com/products/activate/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data.isActive)
			Swal.fire({
				title: 'Successful',
				icon: 'success',
				text: 'Item Activated'
			});
			setItemIsActive(true)
			setItemIsActiveWord('Available')
		})
	}

	function deactivateStatus(_id){
		fetch(`https://secret-meadow-31644.herokuapp.com/products/archiveProduct/${_id}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => 
			{
				console.log(data.isActive)
				Swal.fire({
				title: 'Successful',
				icon: 'success',
				text: 'Item Deactivated'
			});
			setItemIsActive(false)
			setItemIsActiveWord('Not Available')
		})
	}


	return (
		(deleteItem !== true)?
			<>	
	        <tr>
		         <td>{itemName}</td>
		         <td>{itemDescription}</td>
		         <td>{itemPrice}</td>
		         <td>{itemIsActiveWord}</td>
		         <td className = "col-lg-1">
		         	<Button variant="outline-primary" id="btnOptions" className = "w-80 m-2" onClick={handleShowEdit}>Edit</Button>{' '}
		         	<Button variant="outline-danger" id="btnOptions" className = "w-80 m-2" onClick={handleShowDelete}>Delete</Button>{' '}		
		         	
		         	{	itemIsActive ?

		         		<Button variant="outline-warning" id="btnOptions" className = "w-80 m-2" onClick={() => deactivateStatus(_id)}>Deactivate</Button>
		         		:			         	
		         		<Button variant="outline-success" id="btnOptions" className = "w-80 m-2" onClick={() => activateStatus(_id)}>Activate</Button>		
		         	}	         	

		         </td>
		    </tr>


			<Modal show={showEdit} onHide={handleCloseEdit} >
			        <Modal.Header closeButton>
			        <Modal.Title>Edit Product's Info</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form>
							<Form.Group className="mb-3" controlId="productName">
						    <Form.Label>Name</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder=" "
						        value = {modalName}
						        onChange = {e => setModalName(e.target.value)}
						        autoFocus
						    />
							</Form.Group>

						    <Form.Group className="mb-3" controlId="productName">
						    <Form.Label>Description</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder=" "
						        value = {modalDescription}
						        onChange = {e => setModalDescription(e.target.value)}
						        autoFocus
						    />
						    </Form.Group>

						    <Form.Group className="mb-3" controlId="productPrice">
						    <Form.Label>Price</Form.Label>
						    <Form.Control
						        type="number"
						        placeholder=" "
						        value = {modalPrice}
						        onChange = {e => setModalPrice(e.target.value)}
						        autoFocus
						    />
						    </Form.Group>

						</Form>
						</Modal.Body>
					<Modal.Footer>

						{ (modalName === itemName && modalDescription === itemDescription && modalPrice === itemPrice)?
							<Button variant="danger" onClick={() => editProduct(_id)} disabled>
							Save Changes
							</Button>
							:
							<Button variant="primary" onClick={() => editProduct(_id)}>
							Save Changes
							</Button>
						}
							
					</Modal.Footer>
			</Modal>  

			<Modal
			       show={showDelete}
			       onHide={handleCloseDelete}
			       backdrop="static"
			       keyboard={false}
			     >
			       <Modal.Header closeButton>
			         <Modal.Title>Do you want to delete this item?</Modal.Title>
			       </Modal.Header>
			       <Modal.Body>
			         If you delete this item, you will no longer retrieve it form your database.
			       </Modal.Body>
			       <Modal.Footer>
			         <Button variant="secondary" onClick={handleCloseDelete}>
			           Close
			         </Button>
			         <Button variant="primary" onClick={() => deleteProduct(_id)}>Delete</Button>
			       </Modal.Footer>
			</Modal>  
			</>
			:
			deleteProduct(_id)
			
		)
}