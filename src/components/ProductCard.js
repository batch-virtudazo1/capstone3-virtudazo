import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {

	const { name, description, price, _id} = productProp;

	return (
			<Card  className = "productCard">
			     <Card.Body>
			        <Card.Title>{name}</Card.Title>
			      	<Card.Subtitle>Description:</Card.Subtitle>
			        <Card.Text>
						{description}
			        </Card.Text>
			        <Card.Subtitle>Price:</Card.Subtitle>
			        <Card.Text>{price}</Card.Text>
			        <Link className = "btn btn-primary" to = {`/productView/${_id}`}>View Details</Link>
			    </Card.Body>
			    </Card>
		)
}