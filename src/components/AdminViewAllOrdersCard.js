import {useEffect, useState} from 'react';
import {Button, Col, Form, ListGroup, Modal, Row, Table} from 'react-bootstrap';


export default function AdminViewAllOrdersCard({ordersProp}){

    const {firstName, lastName, mobileNo, isAdmin, orders, _id} = ordersProp;

    // Modals
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    const [productsItem, setProductsItem] = useState(ordersProp.orders);
    
    const [productsGetter, setProductsGetter] = useState('');
    const [dataItem, setDataItem] = useState('');
    const [orderId, setOrderId] = useState([]);
    const [details, setDetails] = useState([]);

    let orderHolder = [];
    let productsHolder = [];
    let totalAmount = 0;
    let allItemNames = [];
    let itemIdHolder = [];
    let itemNameHolderArr = [];
    let dateHolder = [];
    let amountHolder = [];
    let orderIdHolder = [];
    let viewItems = [];
    let quantity = 0;

    
    // this loop is for getting the total amount and order's info
    for(let index = 0; index < orders.length; index ++){
        orderHolder = orders[index]

        for(let i = 0; i < orderHolder.products.length; i++){
            productsHolder = orderHolder.products[i]
            orderIdHolder.push(orderHolder._id);
            allItemNames.push(orderHolder.products[i].productName)
            dateHolder.push(orderHolder.purchaseOn)
            amountHolder.push(orderHolder.totalAmount)
            quantity = quantity + productsHolder.quantity;

        }
        totalAmount = totalAmount + orderHolder.totalAmount;
        itemIdHolder.push(productsHolder.productId)
        
    }

    // View All Items
    for(let iterate = 0; iterate < itemIdHolder.length; iterate++){
        viewItems.push(`(Order Id: 
            ${orderIdHolder[iterate]} ,
            Product Number: 
            ${itemIdHolder[iterate]} ,
            Product Name: 
            "${allItemNames[iterate]}" ,
            Quantity: 
            x${productsHolder.quantity} ,
            Amount:
            Php. ${amountHolder[iterate]} ,
            Date Purchased:
            ${dateHolder[iterate]}) `)
    }

    return(
        <>
        <tr >
            <td>{`${firstName} ${lastName}`}</td>
            <td>{mobileNo}</td>     
            <td>{quantity}</td>             
            <td>{`Php ${totalAmount}`}</td> 
            <td>{viewItems}</td>
        </tr>
        </>

    )
}