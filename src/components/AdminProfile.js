import {useContext, useEffect, useState} from 'react';
import {Table} from 'react-bootstrap';
import UserContext from '../UserContext';

export default function AdminProfile(){

	const {user, setUser} = useContext(UserContext);
	console.log(user);

	const [userName, setUserName] = useState('');
	const [userContact, setContact] = useState('');
	const [userEmail, setEmail] = useState('');

	// useEffect to fetch the user details
	useEffect(() => {
		fetch("https://secret-meadow-31644.herokuapp.com/users/getUserDetails", {
			method: 'GET',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`

				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setUserName(`${data.firstName} ${data.lastName}`)
				setContact(data.mobileNo)
				setEmail(data.email)
			});
	})
	
	return(

			<Table striped bordered hover className="mt-3">
			      <thead>
			        <tr>
			          <th colSpan={2} className = "text-center " >Admin Profile</th>
			        </tr>
			      </thead>
			      <tbody>
			        <tr>
			          <td>Name:</td>
			          <td>{userName}</td>
			        </tr>
			        <tr>
			          <td>Contact:</td>
			          <td>{userContact}</td>
			        </tr>
			        <tr>
			          <td>Email:</td>
			          <td>{userEmail}</td>
			        </tr>
			      </tbody>
			</Table>
			
		)
}