import {useContext} from 'react';
import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Banner(){
	const {user} = useContext(UserContext);

	return (
			user.isAdmin !== true ?
			<Row>
				<Col className = "p-5">
					<h1>Imah's Dressmaking Shop</h1>
					<p>Shop and Wear customized and personalized design.</p>
					<Link className = "btn btn-primary" as={Link} to = "/products">Shop Now</Link>
				</Col>
			</Row>
			:
			<Row>
				<Col className = "p-5">
					<h1>Imah's Dressmaking Shop</h1>
					<p>Shop and Wear customized and personalized design.</p>
				</Col>
			</Row>
		)
}