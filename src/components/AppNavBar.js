import {useContext} from 'react';
import {Link} from 'react-router-dom';
import {Container, Image, Navbar, Nav} from 'react-bootstrap';
import Logo from '../images/SampleLogo.png';
import UserContext from '../UserContext';
export default function AppNavBar(){

	const {user} = useContext(UserContext);

	return (
		<Navbar  expand="lg">
		      <Container>
		        <Navbar.Brand as = {Link} to = "/">
		        <Image src={Logo} className="img-fluid m-auto " width="50" height="50"/>
		        </Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="me-auto">
		            
		            { user.id === null ?
		            	<>
		            	<Nav.Link as = {Link} to = "/">Home</Nav.Link>
	            		<Nav.Link as = {Link} to = "/products">Products</Nav.Link>
		            	</>
		            	:
		            	<>
		            	<Nav.Link as = {Link} to = "/">Home</Nav.Link>
		            	<Nav.Link as = {Link} to = "/profile">Profile</Nav.Link>
	            		<Nav.Link as = {Link} to = "/products">Products</Nav.Link>
	            		<Nav.Link as = {Link} to = "/services">Services</Nav.Link>
	            		</>
		            }
		            	
		         		                
		                {
		                	(user.id !== null) ?
		                	<Nav.Link as = {Link} to = "/logout">Logout</Nav.Link>
		                	:
		                	<>
		                	<Nav.Link as = {Link} to = "/login">Login</Nav.Link>
		                	<Nav.Link as = {Link} to = "/register">Register</Nav.Link>
		              		</>
		              	}
		            
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
	)
};