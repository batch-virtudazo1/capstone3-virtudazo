import { Card, Col, Image, Row} from 'react-bootstrap';
import ProductSample from '../images/productSample.png';
export default function Highlights(){
	return (
		<section>
			<Row className = "mt-3 mb-3">
				<Col xs={12} md={4} >
					<Card className = "cardHighlight p-3 border border-5 border-info rounded" >
						<Card.Body>
							<Card.Title className="p-3 border border-5 border-success rounded">
								<h2>Customize your OOTD!</h2>
							</Card.Title>
							<Card.Text>
								<Image src={ProductSample} className="img-fluid "roundedCircle/>
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className = "cardHighlight p-3 border border-5 border-info rounded">
						<Card.Body>
							<Card.Title className="p-3 border border-5 border-success rounded">
								<h2>Shop at a very affordable price</h2>
							</Card.Title>
							<Card.Text>
								<Image src={ProductSample} className="img-fluid "roundedCircle/>
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>

				<Col xs={12} md={4}>
					<Card className = "cardHighlight p-3 border border-5 border-info rounded">
						<Card.Body>
							<Card.Title className="p-3 border border-5 border-success rounded">
								<h2>Wear a designer-like outfit!</h2>
							</Card.Title>
							<Card.Text>
								<Image src={ProductSample} className="img-fluid "roundedCircle/>
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</section>
		)
}