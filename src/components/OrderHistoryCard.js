import {useState} from 'react';
export default function OrderHistoryCard({userOrdersProp}){
	console.log(userOrdersProp)

 	const {purchaseOn, totalAmount, _id} = userOrdersProp;
 	
	return(
			userOrdersProp.products.map((product) =>
			    <tr>
			        <td>{purchaseOn}</td>
			        <td>{_id}</td>
			        <td>{product.productName}</td>
			        <td>{totalAmount}</td>
			        <td>{product.quantity}</td>
			    </tr>
			)
		)
}