import {useContext, useEffect, useState} from 'react';
import {Table} from 'react-bootstrap';
import OrderHistoryCard from '../components/OrderHistoryCard';
import UserContext from '../UserContext';


export default function UserProfile(){
	
	const {user, setUser} = useContext(UserContext);
	console.log(user);

	const [userName, setUserName] = useState('');
	const [userContact, setContact] = useState('');
	const [userEmail, setEmail] = useState('');
	const [userOrders, setUserOrders] = useState([]);


	useEffect(() => {
		fetch("https://secret-meadow-31644.herokuapp.com/users/getUserDetails", {
			method: 'GET',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`

				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)
				setUserName(`${data.firstName} ${data.lastName}`)
				setContact(data.mobileNo)
				setEmail(data.email)

			});

	})

	useEffect(() => {
		fetch("https://secret-meadow-31644.herokuapp.com/users/getUserOrders", {
			method: 'GET',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`

			}

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUserOrders(data.map(userOrder =>{
				return(
					
						<OrderHistoryCard key={userOrder._id} userOrdersProp = {userOrder}/>	
					
					)
			}));

		})
	}, [])

	return(
		<>
		<Table striped bordered hover>
		      <thead>
		        <tr>
		          <th colSpan={2} className = "text-center">Profile</th>
		        </tr>
		      </thead>
		      <tbody>
		        <tr>
		          <td>Name:</td>
		          <td>{userName}</td>
		        </tr>
		        <tr>
		          <td>Contact:</td>
		          <td>{userContact}</td>
		        </tr>
		        <tr>
		          <td>Email</td>
		          <td>{userEmail}</td>
		        </tr>
		      </tbody>
		 </Table>


		 <Table striped bordered hover>
		       <thead>
		       		<tr>
		       		  <th colSpan={5} className = "text-center">Order History</th>
		       		</tr>	
		       </thead>
		       <tbody>
			       <tr>
				       <td>Date Purchased</td>
				       <td>Order Id</td>
				       <td>Items</td>
				       <td>Total Amount</td>
				       <td>Quantity</td>
			       </tr>
			       {userOrders} 
		       </tbody>
		  </Table>
		  </>
		)
}