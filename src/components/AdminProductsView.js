import {useContext, useEffect, useState} from 'react';
import {Button,  Form, Modal,  Table, Tab, Tabs} from 'react-bootstrap';

import AdminProductsViewCard from '../components/AdminProductsViewCard';
import AdminViewAllOrdersCard from '../components/AdminViewAllOrdersCard';
import ProductView from '../components/ProductView';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function AdminProductsView(){

	const {user, setUser} = useContext(UserContext);
	console.log(user);
	
	const [products, setProducts] = useState([]);
	const [orders, setOrders] = useState([]);
	
	// for not forcing to refresh the page when adding a product
	//const [addedProduct, setAddedProduct] = useState(false);

	// for the button adding product
	const [ isActiveBtn , setIsActiveBtn] = useState(false);

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	// Modals
	const [showAdd, setShowAdd] = useState(false);

	const handleCloseAdd = () => setShowAdd(false);
	const handleShowAdd =  () => setShowAdd(true);

// useEffect for all products 
	useEffect(() => {
		fetch("https://secret-meadow-31644.herokuapp.com/products", {
			method: 'GET',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`

			}

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product =>{
				return(
					
						<AdminProductsViewCard key={product._id} productsProp = {product}/>	
					
					)
			}));

		})
	}, [])


// useEffect for all users orders
useEffect(() => {
	fetch("https://secret-meadow-31644.herokuapp.com/users/getAllUsersOrders", {
		method: 'GET',
		headers: {
			'Content-Type' : 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`

		}

	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		setOrders(data.map(order =>{
			return(
					
					<AdminViewAllOrdersCard key={order._id} ordersProp = {order}/>	
					
				)
		}));

	})
}, [])


	function addProduct(){
		fetch('https://secret-meadow-31644.herokuapp.com/products/addProduct', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			Swal.fire({
				title: 'Success!',
				icon: 'success',
				text: 'Product Added!'
			});
			//setAddedProduct(true);
			setShowAdd(false);
			setName('');
			setDescription('');
			setPrice(0);

		})

	}



	useEffect(() => {
	     
	        if(name !== '' && description !== '' && price !== 0){
	            setIsActiveBtn(true);

	        } else {
	            setIsActiveBtn(false);
	        }


	    }, [name, description, price]);


	return(

			(user.id === null  )?	
			<ProductView/>
			:
			
			<>			
			<h1 className = "text-center">Admin Panel</h1>
			<Tabs
			     defaultActiveKey="productsInfo"
			     id="justify-tab-example"
			     className="mb-3"
			     justify
			>

			<Tab eventKey="productsInfo" title="Products Info">
					
					
					<Table striped bordered hover>
						<thead>
						    <tr>
							   	  <th>Item Name</th>
						      	  <th>Description</th>
						      	  <th>Unit Price</th>
						      	  <th>Status</th>
						      	  <th>Options</th>
						    </tr>
						</thead>    
						    	<tbody>
						  			{products}
							 	</tbody>
							 
						</Table>
					
						
						{
						<Button id = "addProductBtn" type = "submit" variant = "success" onClick={handleShowAdd}>
							Add Product
						</Button>
						}

					<Modal show={showAdd} onHide={handleCloseAdd} >
						        <Modal.Header closeButton>
						        <Modal.Title>Add Product's Info</Modal.Title>
								</Modal.Header>
								<Modal.Body>
									<Form>
										<Form.Group className="mb-3" controlId="productName">
									    <Form.Label>Name</Form.Label>
									    <Form.Control
									        type="text"
									        placeholder=" "
									        value = {name}
									        onChange = {e => setName(e.target.value)}
									        autoFocus
									    />
										</Form.Group>

									    <Form.Group className="mb-3" controlId="productDescription">
									    <Form.Label>Description</Form.Label>
									    <Form.Control
									        type="text"
									        placeholder=" "
									        value = {description}
									        onChange = {e => setDescription(e.target.value)}
									        autoFocus
									    />
									    </Form.Group>

									    <Form.Group className="mb-3" controlId="productPrice">
									    <Form.Label>Price</Form.Label>
									    <Form.Control
									        type="number"
									        placeholder=" "
									        value = {price}
									        onChange = {e => setPrice(e.target.value)}
									        autoFocus
									    />
									    </Form.Group>

									</Form>
									</Modal.Body>
								<Modal.Footer>
							
								    <Button variant="secondary" onClick={handleCloseAdd}>
								    Close
								    </Button>
								{ (isActiveBtn ) ?
								    <Button variant="primary" onClick={() => addProduct()}>
								    Add Product
								    </Button>
								:
									<Button variant="danger" type="submit" disabled>
									Add Product
									</Button>
								}
								</Modal.Footer>
						</Modal>  

			  	  </Tab>
			  	  

				<Tab eventKey="viewOders" title="View All Users Orders">
				   <Table striped bordered hover>
				        <thead>
				           <tr>
				         	  <th>Name</th>
				         	  <th>Contact No.</th>
				         	  <th>Total Quantity</th>
				         	  <th>Total Amount</th>
				         	  <th>Order Details</th> 
				           </tr>
				        </thead>
				        <tbody>
				   		{orders}
				   		
				   		</tbody>
				   </Table>
				</Tab>
			</Tabs>	

	
			</>
			
		)
}